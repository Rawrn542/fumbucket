{
    "id": "f35e5da9-2f18-4246-b57f-e6e24ef1e236",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d590856-c18d-41cf-821b-98a9a31e1e78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f35e5da9-2f18-4246-b57f-e6e24ef1e236",
            "compositeImage": {
                "id": "c373ee5e-60d3-45fd-b340-7e14e65c9c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d590856-c18d-41cf-821b-98a9a31e1e78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc3e71a0-579c-483b-9e1e-5d29397d879e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d590856-c18d-41cf-821b-98a9a31e1e78",
                    "LayerId": "b65105cb-a554-415f-adfa-ca424f1c221f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "b65105cb-a554-415f-adfa-ca424f1c221f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f35e5da9-2f18-4246-b57f-e6e24ef1e236",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}