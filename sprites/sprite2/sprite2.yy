{
    "id": "70ba673f-a557-4e96-aecd-8dbdc8e2cb3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d027385-5bb3-40fb-9cbc-a0a474d69e10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70ba673f-a557-4e96-aecd-8dbdc8e2cb3e",
            "compositeImage": {
                "id": "55f79639-fa37-42f6-9ffc-bef4a6261d85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d027385-5bb3-40fb-9cbc-a0a474d69e10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a6868fb-d376-408b-82a8-bbf02e9e13dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d027385-5bb3-40fb-9cbc-a0a474d69e10",
                    "LayerId": "79ab9240-e681-4cc4-8ec8-e812847c7ec4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "79ab9240-e681-4cc4-8ec8-e812847c7ec4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70ba673f-a557-4e96-aecd-8dbdc8e2cb3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}